import 'package:feature_ndid_module/config/route/route_path.dart';
import 'package:feature_ndid_module/main.dart';
import 'package:feature_ndid_module/views/ndid_detail_view.dart';
import 'package:feature_ndid_module/views/ndid_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppRoute{
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case RoutePath.main :
        return MaterialPageRoute(settings: const RouteSettings(name: RoutePath.main),
          builder: (_)=> const MyHomePage(title: 'Test'));
      case RoutePath.ndidView :
        return MaterialPageRoute(builder: (_)=> const NDIDView());
      case RoutePath.ndidDetail :
        final argument = settings.arguments as int;
        return MaterialPageRoute(builder: (_)=> NDIDDetailView(valueNDID: argument));
      default:
        return MaterialPageRoute(
          builder: (_) => const Scaffold(
            body: Center(
              child: Text(''),
            ),
          ),
        );
    }
  }
}