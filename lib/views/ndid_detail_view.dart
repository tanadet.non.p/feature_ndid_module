import 'package:flutter/material.dart';

class NDIDDetailView extends StatefulWidget {
  final int valueNDID;

  const NDIDDetailView({super.key, required this.valueNDID});

  @override
  State<NDIDDetailView> createState() => _NDIDDetailViewState();
}

class _NDIDDetailViewState extends State<NDIDDetailView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          onPressed: ()=> Navigator.of(context).pop(),
          icon: const Icon(Icons.arrow_back_ios),
        ),
        title: const Text("NDID Detail"),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        child: Center(
          child: Text('Detail NDID is : ${widget.valueNDID}', style: const TextStyle(fontSize: 40, color: Colors.black, fontWeight: FontWeight.w500),),
        )
      ),
    );
  }
}
