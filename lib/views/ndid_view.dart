import 'package:feature_ndid_module/widget/ndid_item_widget.dart';
import 'package:flutter/material.dart';

class NDIDView extends StatefulWidget {
  const NDIDView({super.key});

  @override
  State<NDIDView> createState() => _NDIDViewState();
}

class _NDIDViewState extends State<NDIDView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          onPressed: ()=> Navigator.of(context).pop(),
          icon: const Icon(Icons.arrow_back_ios),
        ),
        title: const Text("Feature NDID"),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            const Text(
              'NDID LIST :', style: TextStyle(fontSize: 30, color: Colors.black, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 10,),
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: 30,
                  itemBuilder: (context, index) => NDIDItemWidget(indexItem: index+1)
              ),
            )
          ],
        ),
      ),
    );
  }
}
