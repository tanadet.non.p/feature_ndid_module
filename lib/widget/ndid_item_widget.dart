import 'package:feature_ndid_module/config/route/route_path.dart';
import 'package:feature_ndid_module/views/ndid_detail_view.dart';
import 'package:flutter/material.dart';

class NDIDItemWidget extends StatelessWidget {
  final int indexItem;

  const NDIDItemWidget({super.key, required this.indexItem});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()=> Navigator.of(context).pushNamed(RoutePath.ndidDetail, arguments: indexItem),
      child: Container(
        margin: const EdgeInsets.only(top: 10),
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: indexItem % 2 == 0 ? Colors.grey : Colors.grey.shade400
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Item NDID : $indexItem'),
            const SizedBox(height: 5,),
            Text('Detail NDID : Description About NDID $indexItem')
          ],
        ),
      ),
    );
  }
}
